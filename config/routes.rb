Rails.application.routes.draw do
  resources :links, only: [:create, :show, :new, :index]
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  
  get "/:shorter_link", to: "links#show"
  root to: "links#new"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
