class LinksController < ApplicationController
  before_action :find_link, only: [:show]
  before_action :user_sign_in, only: [:new]
  def index
    @link = Link.find_by(id: params[:id])
    unless @link
      redirect_to root_path
    end
  end

  def show
    @link = Link.find_by_shorter_link(params[:shorter_link])
    redirect_to redirect_link(@link.original_link)
  end

  def new
    
  end

  def create
    @link = Link.new(link_params)
    respond_to do |format|
      if @link.save
        format.html { redirect_to links_path(id: @link.id), notice: 'Link was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end  

  private

    def find_link
      @link = Link.find_by_shorter_link(params[:shorter_link])
    end
    
    def link_params
      params.require(:link).permit(:original_link)
    end

    def user_sign_in
      if user_signed_in?
        @link = Link.new
      else
        redirect_to user_session_path
      end
    end

    def redirect_link(link)
      url = 'https://'+link
      url
    end
end
