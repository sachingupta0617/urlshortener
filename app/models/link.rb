class Link < ApplicationRecord

	validates :original_link, presence: true, on: :create
  	validates_format_of :original_link,
    with: /\A(?:(?:http|https):\/\/)?([-a-zA-Z0-9.]{2,256}\.[a-z]{2,4})\b(?:\/[-a-zA-Z0-9@,!:%_\+.~#?&\/\/=]*)?\z/
	
	before_create :generate_shorter_link
	
	def generate_shorter_link
		chars = ['0'..'9','A'..'Z','a'..'z'].map{|range| range.to_a}.flatten
	    self.shorter_link = 8.times.map{chars.sample}.join
	    self.shorter_link = 8.times.map{chars.sample}.join until Link.find_by_shorter_link(self.shorter_link).nil?
	end
end
